/* eslint-disable no-unused-vars */
/*!

=========================================================
* Vue Argon Dashboard - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';
import ArgonDashboard from './plugins/argon-dashboard';
import store from './store/store';
import axios from 'axios';
import VueSweetalert2 from 'vue-sweetalert2';

Vue.prototype.$http = axios;
const token = localStorage.getItem ('token');
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] =
    'fastm ' + token;
}
Vue.config.productionTip = false;
Vue.use (ArgonDashboard);
Vue.use (VueSweetalert2);

new Vue ({
  router,
  store,
  vuetify,
  render: h => h (App),
}).$mount ('#app');
