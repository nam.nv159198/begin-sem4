/* eslint-disable no-unused-vars */
import Vue from 'vue';
import Vuex from 'vuex';
import Student from './modules/student';
import Auth from './modules/auth';
import School from './modules/school';
import Users from './modules/user';
import createPersistedState from "vuex-persistedstate";

Vue.use (Vuex);
export default new Vuex.Store ({
  modules: {
    student : Student,
    auth : Auth,
    school : School,
    user : Users
  },
   plugins: [createPersistedState()],
});
