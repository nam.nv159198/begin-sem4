import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use (Vuex);

const url = 'http://duyphamdev.xyz:8111/usermanager/user';

// eslint-disable-next-line no-unused-vars
const studentStore = {
  namespaced: true,

  state: {
    users: [],
    user: {},
  },

  // eslint-disable-next-line no-unused-vars
  getters: {
    getStudentList: state => {
      return state.students;
    },
  },

  // eslint-disable-next-line no-unused-vars
  mutations: {
    FETCH (state, users) {
      state.users = users;
    },
    FETCH_ONE (state, student) {
      state.student = student;
    },
  },

  actions: {
    fetch({commit}) {
      return axios
        .get (url + '/?start=0&limit=5')
        .then (response => commit ('FETCH', response.data.data))
        .catch ();
    },

    // fetchOne ({commit}, id) {
    //   axios
    //     .get (`${url}/${id}`)
    //     .then (response => commit ('FETCH_ONE', response.data.data))
    //     .catch ();
    // },

    // // eslint-disable-next-line no-empty-pattern
    // deleteStudent ({}, id) {
    //   axios
    //     .delete (`${url}/${id}`)
    //     .then (response => response.status)
    //     .catch (response => response.status);
    // },
    // // eslint-disable-next-line no-empty-pattern
    // editStudent ({}, student) {
    //   axios ({
    //     url: `${url}/${student.id}`,
    //     data: student,
    //     method: 'PUT',
    //   }).then (response => response.status);
    // },
    // // eslint-disable-next-line no-empty-pattern
    // addStudent ({}, student) {
    //   axios
    //     .post (url, student)
    //     .then (response => response.status)
    //     .catch (response => response.status);
    // },
  },
};

export default studentStore;
