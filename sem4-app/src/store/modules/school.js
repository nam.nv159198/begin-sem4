import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use (Vuex);

// eslint-disable-next-line no-unused-vars
const url = 'http://duyphamdev.xyz:8111/school/';

// eslint-disable-next-line no-unused-vars
const schoolStore = {
 namespaced: true,

  state: {
    schools: [],
    school: {},
  },

  // eslint-disable-next-line no-unused-vars
  getters: {
    
  },

  // eslint-disable-next-line no-unused-vars
  mutations: {
    FETCH (state, schools) {
      state.schools = schools;
    },
    // FETCH_ONE (state, school) {
    //   state.student = school;
    // },
  },

  actions: {
    fetch({commit}) {
      return axios
        .get (url + 'getall')
        .then (response => commit ('FETCH', response.data.data))
        .catch ();
    },

    // fetchOne ({commit}, id) {
    //   axios
    //     .get (`${url}/${id}`)
    //     .then (response => commit ('FETCH_ONE', response.data.data))
    //     .catch ();
    // },

    // eslint-disable-next-line no-empty-pattern
    // deleteStudent ({}, id) {
    //   axios
    //     .delete (`${url}/${id}`)
    //     .then (response => response.status)
    //     .catch (response => response.status);
    // },
    // // eslint-disable-next-line no-empty-pattern
    // editStudent ({}, student) {
    //   axios({
    //       url: `${url}/${student.id}`,
    //       data: student,
    //       method: 'PUT'
    //     })
    //     .then (response => response.status);
    // },
    // eslint-disable-next-line no-empty-pattern
    addSchool ({}, school) {
      axios
        .post(url + 'create', school)
        .then (res => console.log(res.status))
        .catch (res => console.log(res.status))
    },
  },
}

export default schoolStore;
