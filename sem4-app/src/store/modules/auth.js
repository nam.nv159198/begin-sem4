import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
// eslint-disable-next-line no-unused-vars
const url = 'http://duyphamdev.xyz:8111/api/auth/signin/';

Vue.use (Vuex);

const authStore = {

  namespaced: true,

  state: {
    status: '',
    username: '',
    email:  '',
    roles:  [],
    menus : [],
    token: localStorage.getItem ('token') || '',
  },

  // eslint-disable-next-line no-unused-vars
  mutations: {
    SET_USERNAME (state, username) {
      state.username = username;
    },
    SET_EMAIL (state, email) {
      state.email = email;
    },
    SET_TOKEN (state, token) {
      state.status = 'success';
      state.token = token;
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles;
    },
    SET_MENU : (state,menus) => {
        state.menus = menus;
    },
    LOGOUT (state) {
      state.status = '';
      state.token = '';
      state.username  = '';
      state.roles = [];
      state.menus = [];
      state.email = [];
    },
  },

  // eslint-disable-next-line no-unused-vars
  actions: {
    login ({commit}, user) {
      return new Promise ((resolve, reject) => {
        axios ({
          url: url,
          data: user,
          method: 'POST',
        })
          .then (resp => {
            const token = resp.data.data.accessToken;
            const username = resp.data.data.username;
            const email  = resp.data.data.email;
            const roles = resp.data.data.roles;
            const menus = resp.data.data.listMenuAction;
            localStorage.setItem('token', token);
            axios.defaults.headers.common['Authorization'] = 'fastm ' + token;
            commit('SET_TOKEN', token);
            commit ('SET_USERNAME', username);
            commit ('SET_EMAIL',  email);
            commit ('SET_ROLES',  roles);
            commit('SET_MENU' ,menus);
            resolve (resp);
          })
          .catch (err => {
            localStorage.removeItem('token');
            reject (err);
          });
      });
    },
 
     logout({commit}) {
      // eslint-disable-next-line no-unused-vars
      return new Promise ((resolve, reject) => {
        commit ('LOGOUT');
        localStorage.removeItem ('token');
        localStorage.removeItem('vuex');
        delete axios.defaults.headers.common['Authorization'];
        resolve ();
      });
    },
  },

  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    getRoles: state => state.roles,
    getUser: state => state.username,
    getEmail: state => state.email,
    menuList : state => state.menus 
  }

};

export default authStore;
