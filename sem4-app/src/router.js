import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
import store from './store/store';

Vue.use(Router)


let router =  new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
      meta: {
        requiresAuth: true
        },

      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue')
        },
        {
          path: '/icons',
          name: 'icons',
          component: () => import(/* webpackChunkName: "demo" */ './views/Icons.vue')
        },
        {
          path: '/profile',
          name: 'profile',
          component: () => import(/* webpackChunkName: "demo" */ './views/UserProfile.vue')
        },
        {
          path: '/maps',
          name: 'maps',
          component: () => import(/* webpackChunkName: "demo" */ './views/Maps.vue')
        },
        {
          path: '/tables',
          name: 'tables',
          component: () => import(/* webpackChunkName: "demo" */ './views/Tables.vue')
        },
           {
          path: '/students',
          name: 'students',
          component: () => import(/* webpackChunkName: "demo" */ './views/Tables/Students/Tables.vue')
          },
          {
          path: '/schools',
          name: 'schools',
          component: () => import(/* webpackChunkName: "demo" */ './views/Tables/Schools/Tables.vue')
          },
          {
          path: '/users',
          name: 'users',
          component: () => import(/* webpackChunkName: "demo" */ './views/Tables/Users/Tables.vue')
          }
      ]
    },
    {
      path: '/',
      redirect: 'login',
      component: AuthLayout,
      children: [
        {
          path: '/login',
          name: 'login',
          component: () => import(/* webpackChunkName: "demo" */ './views/Login.vue')
        },
        {
          path: '/register',
          name: 'register',
          component: () => import(/* webpackChunkName: "demo" */ './views/Register.vue')
        }
      ]
    }
  ],
  // eslint-disable-next-line no-unused-vars
  scrollBehavior (to, from, savedPosition) {
  // eslint-disable-next-line no-unused-vars
  return new Promise((resolve, _reject) => {
    setTimeout(() => {
      resolve({ x: 0, y: 0 })
    }, 800)
  })
}
})

router.beforeEach((to, from, next) => {
  if (to.matched.some (record => record.meta.requiresAuth)) {
    if (store.getters['auth/isLoggedIn']) {
      next ();
      return;
    }
    next ('/login');
  } else {
    next ();
  }
});

export default router;
